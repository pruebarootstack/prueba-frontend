import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BerbecuesService } from '../shared/services/barbecues.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Barbecue } from '../shared/models/barbecue';
import { UserService } from '../shared/services/user.service';
declare var $: any;

@Component({
    selector: 'home',
    templateUrl: './home.html',
    providers: [UserService],
    styleUrls: ['./home.css']
})
export class HomeComponent implements OnInit {

    barbecues: Array<Barbecue> = [];
    latitud: number;
    longitud: number;
    user: any;
    lat: number = 51.678418;
    lng: number = 7.809007;

    constructor(private router: Router, private barbeService: BerbecuesService, private userService: UserService) {
        this.user = userService.getUser();
    }

    ngOnInit(): void {

        if (navigator && navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((res) => {
                console.log(res.coords.latitude);
                this.latitud = res.coords.latitude;
                this.longitud = res.coords.longitude;
                localStorage.setItem('positionUser', JSON.stringify({ latitud: this.latitud, longitud: this.longitud }));
                this.getBarbecueLatLong();
            }, this.geo_error);
        } else {
            alert("Geo no es soportado, se muestra el listado completo de barbacoas");
        }

        this.getAllBarbecue();
    }

    getBarbecueLatLong() {
        this.barbeService.getBerbecueLatLong(this.latitud, this.longitud).subscribe(res => {
            this.barbecues = res['data'];
            console.log(res);
        }, (errorResponse: HttpErrorResponse) => {
            console.log(errorResponse);
        });
    }

    getAllBarbecue() {
        this.barbeService.getBarbecues().subscribe(res => {
            this.barbecues = res['data'];
            console.log(res);
        }, (errorResponse: HttpErrorResponse) => {
            console.log(errorResponse);
        });
    }

    geo_error(err) {
        if (err.code == 1) {
            console.log('El usuario no quiere mostrar su localización. Se procede a listar todas las barbacoas.');
        } else if (err.code == 2) {
            alert('La información es innacessible.')
        } else if (err.code == 3) {
            alert('La petición ha durado demasiado tiempo.')
        } else {
            alert('Se ha producido un error inesperado.')
        }
    }

    alquilar(barbecue: number) {
        if (confirm("Esta seguro que desea alquilar esta barbacoa?")) {
            this.barbeService.rentBarbecue(barbecue, this.user.sub).subscribe(res => {
                if (res['statusCode'] == 404) {
                    alert(res['message']);
                    return;
                } else if (res['statusCode'] == 200) {
                    alert("Has rentado la barbacoa. Gracias por su alquiler.");
                    this.verificarLatLong();
                    return;
                } else {
                    alert("Error en la solicitud. Intente en otro momento");
                    return;
                }
            });
        }
        return;
    }

    verificarLatLong() {
        if (this.latitud == null && this.longitud == null) {
            this.getAllBarbecue();
            return;
        }
        this.getBarbecueLatLong();
    }

    viewMap(latitud: number, longitud: number, event:any){
        event.preventDefault();
        this.lat = latitud;
        this.lng = longitud;
    }

    notify(message, type) {
        $.notify({
            // options
            message: message
        }, {
                // settings
                type: type
            });
    }
    
}
