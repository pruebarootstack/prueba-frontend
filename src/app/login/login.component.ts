import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from '../shared/services/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

declare var $:any

@Component({
    selector: 'login',
    templateUrl: './login.html',
    styleUrls: ['./login.css'],
})
export class LoginComponent implements OnInit {

    form: FormGroup;
    isLoaging: boolean;


    constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router) { }

    ngOnInit(): void {

        if (this.userService.check() && !this.userService.isTokenExpired()) {
            this.router.navigate(['home']);
        }

        this.form = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]]
        })
    }

    login(){
        if (this.form.invalid) {
            alert('Debe llenar correctamente los campos');
            return;
        }
        this.isLoaging = true;
        this.userService.login(this.form.value).subscribe(res => {
            this.isLoaging = false;
            if (res['statusCode'] == 404) {
                alert(res['message']);
                return;
            }

            if (res['statusCode'] == 200) {
                this.router.navigate(['home']);
            }

        }, (errorResponse: HttpErrorResponse) => {
            this.isLoaging = false;
            console.log(errorResponse);
        })
    }

    goRegister(){
        this.router.navigate(['auth/register']);
    }

}

