import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        const requestUrl: Array<any> = req.url.split('/');
        const apiURL: Array<any> = environment.apiURL.split('/');
        const token = localStorage.getItem('token');
        if (token && (requestUrl[2] === apiURL[2])) {
            const newRequest = req.clone({ setParams: { 'token': `${token}` } });
            return next.handle(newRequest);        
        }else{
            return next.handle(req);
        }
    } 
}