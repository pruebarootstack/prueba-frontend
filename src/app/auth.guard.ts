import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './shared/services/user.service';

@Injectable()
export class AuthGuard implements CanActivate {
    
    constructor(private userService: UserService, private router: Router){}

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        if (this.userService.check() && !this.userService.isTokenExpired()) {
            return true;
        }
        this.router.navigate(['auth/login']);
        return false;
    }
}
