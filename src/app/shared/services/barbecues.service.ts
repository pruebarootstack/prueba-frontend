import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class BerbecuesService {

    constructor(private http: HttpClient){}

    getBarbecues(){
        return this.http.get(environment.apiURL +'barbecues');
    }

    getBerbecueLatLong(latitud: number, longitud: number){
        return this.http.post(environment.apiURL+'barbecuelatlong', {latitude: latitud, longitude: longitud});
    }

    rentBarbecue(barbecue: number, user:number){
        return this.http.get(environment.apiURL+'rents/'+barbecue+'/'+user);
    }

}