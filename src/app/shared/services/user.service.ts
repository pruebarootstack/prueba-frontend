import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class UserService {

    constructor(private http: HttpClient, private router: Router) { }

    check(): boolean {
        if (localStorage.getItem('token')) {
            return true;
        }
        return false;
    }

    login(credentials: { email: string, password: string }): Observable<boolean> {
        return this.http.post<any>(`${environment.apiURL}auth/login`, credentials)
            .pipe(map(res => res), tap(res => {

                if (res['statusCode'] == 200) {
                    localStorage.setItem('token', res.data.token);
                }

            }));
    }

    register(credentials: {name: string, email: string, password: string, zip_code: string }): Observable<boolean> {
        return this.http.post<any>(`${environment.apiURL}auth/register`, credentials)
            .pipe(map(res => res), tap(res => {
                if (res['statusCode'] == 201) {
                    localStorage.setItem('token', res.data);
                }
            }));
    }

    getUser() {
        return localStorage.getItem('token') ? jwt_decode(localStorage.getItem('token')) : null;
    }

    getTokenExpirationDate(token: string): Date{

        const decode = jwt_decode(token);
        if (decode.exp === undefined) return null;
        const date = new Date(0);
        date.setUTCSeconds(decode.exp);
        return date;
    }

    isTokenExpired(token?: string): boolean{
        if(!token) token = localStorage.getItem('token');
        if(!token) return true;
        const date = this.getTokenExpirationDate(token);
        if(date === undefined) return false;
        return !(date.valueOf() > new Date().valueOf());
    }

}