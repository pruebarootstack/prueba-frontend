export class Barbecue{

    name: string;
    model: string;
    description: string;
    cost: number;
    image: string;
    latitude: number;
    longitude: number;
    available: boolean;
    created_at: Date;
    updated_at: Date;
}