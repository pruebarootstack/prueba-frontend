import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/services/user.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'register',
    templateUrl: './register.html',
    styleUrls: ['./register.css']
})
export class RegisterComponent implements OnInit {

    formRegister: FormGroup;
    isLoading: boolean;

    constructor(private userService: UserService, private router: Router, private formBuilder: FormBuilder) { }

    ngOnInit(): void { 
        
        if (this.userService.check() && !this.userService.isTokenExpired()) {
            this.router.navigate(['home']);
        }

        this.formRegister = this.formBuilder.group({
            name: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]],
            zip_code: ['', [Validators.required]],
        });
    }
    
    goLogin(){
        this.router.navigate(['auth/login']);
    }

    signUp(){
        this.isLoading = true;
        this.userService.register(this.formRegister.value).subscribe(res => {
            console.log('REGISTER', res);
            
            if (res['statusCode'] != 201) {
                alert("Oops!. Un problema en registrar su datos. Intente nuevamente.");
                return;
            }

            setTimeout(() => {
                this.isLoading = false;
                this.router.navigate(['home']);
            }, 2000);

        }, (errorResponse: HttpErrorResponse) => {
            this.isLoading = false;
            console.log(errorResponse);
        })
    }



}
