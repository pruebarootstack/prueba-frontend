import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.html'
})
export class MenuComponent implements OnInit {
    constructor(private router: Router) { }

    ngOnInit(): void { }

    logout(event: any) {
        event.preventDefault();
        if (confirm('Esta seguro de salir?')) {
            localStorage.clear();
            this.router.navigate(['auth/login']);
            return;
        }
        return;
    }
}
